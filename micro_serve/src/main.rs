use anyhow::{Error, Result};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{CreateCollection, Distance, PointStruct, SearchPoints, Value, VectorParams, VectorsConfig};
use actix_web::{web, App, HttpServer};
use std::sync::{Arc, Mutex};
use serde_json::json;
use std::env;
use rand::Rng;
use plotly::{Plot, Scatter};
use plotly::common::Mode;
async fn init_db(collection_name:&String) -> Result<QdrantClient> {
    let client = QdrantClient::from_url("http://localhost:6334").build()?;



    // 创建集合
    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 3,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    println!("Database initialized successfully.");
    Ok(client)
}


// ingest random vector data into the vector database 
async fn ingest_random_data(collection_name:&String,qdrant_client: &QdrantClient) -> Result<(), Error> {

    // Generate random vectors
    let mut vectors = Vec::new();
    for _ in 0..100 {
        let vector: Vec<f32> = (0..3).map(|_| rand::random()).collect();
        vectors.push(vector);
    }

    // Ingest data into the vector database
    let points = vectors
        .iter()
        .enumerate()
        .map(|(i, vector)| PointStruct {
            id: Some((i as u64).into()), // unique u64 or String
            vectors: Some(vector.clone().into()),
            payload: std::collections::HashMap::from([
                ("level".into(), Value::from(9000)),
            ]),
        })
        .collect();
    let response = qdrant_client
        .upsert_points(collection_name, None, points, None)
        .await?;
    println!("Data ingested successfully: {:?}", response);

    Ok(())
}

async fn perform_queries(qdrant_client: &QdrantClient) -> Result<(), Error> {
    // Specify the collection name
    let collection_name = "my_collection";

    // Perform a similarity search query
    let query_vector = vec![0.0_f32; 3];
    let search_result = qdrant_client
        .search_points(
            &SearchPoints {
                collection_name: collection_name.into(),
                vector: query_vector.clone(),
                limit: 10,
                with_payload: Some(true.into()),
                ..Default::default()
            }
        )
        .await?;

    println!("Similarity search result: {:?}", search_result);

    // Perform other types of queries and aggregations as needed

    Ok(())
}

// visualize the query results in a 3D scatter plot
// async fn visualize_results(qdrant_client: &QdrantClient) -> Result<(), Error> {
//     // Specify the collection name
//     let collection_name = "my_collection";

//     // Perform a similarity search query
//     let query_vector = vec![0.0_f32; 3];
//     let search_result = qdrant_client
//         .search_points(
//             &SearchPoints {
//                 collection_name: collection_name.into(),
//                 vector: query_vector.clone(),
//                 limit: 10,
//                 with_payload: Some(true.into()),
//                 with_vectors:Some(true.into()),
//                 ..Default::default()
//             }
//         )
//         .await?;

//     for result in search_result.result {
//         let vector = result.vectors.unwrap();
//         // vector looks like this : Vector: Vectors { vectors_options: Some(Vector(Vector { data: [0.8019409, 0.15225974, 0.5776744], indices: None })) }
//         // vector.vectors_options.unwrap().0.data;
//         let data = vector.vectors_options.unwrap();
//         // now the data looks like this : Vector: Vector(Vector { data: [0.31295744, 0.12929194, 0.9409258], indices: None })
//         // dont know how to get the data inside so weird data structure
        
//         }
    

       
    


//         Ok(())
//     }


#[actix_web::main]
async fn main() -> Result<(), Error> {
 
    
    // 解析命令行参数
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Usage: {} <id> <vector>", args[0]);
        return Ok(());
    }
    let collection_name = args[1].parse().expect("Failed to parse collection name");
   
    let client = init_db(&collection_name).await?;
    ingest_random_data(&collection_name, &client).await?;
    perform_queries(&client).await?;
    // visualize_results(&client).await?;
    Ok(())
}
