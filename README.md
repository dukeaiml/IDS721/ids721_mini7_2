# IDS721_mini7_2

## Dependency
```
actix-web = "4.0.0"
qdrant-client = "1.8.0"
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0.114"
anyhow = "1.0.44"
tokio = { version = "1.0", features = ["full", "rt-multi-thread"] }
tonic = "0.11.0"
rand = "0.8.5"
plotly = "0.8.4"

```
## Ingest data into database
![Alt text](image.png)

I randomly generated 100 vectors with random value and then insert them to the created vector database.

## Query
![Alt text](image-1.png)

According to the cosine similarity, query the database.

## Visulization

Hard to parse the search results, so weird data structure, no clue to get the data inside the VectorOptions.

Vector: Vector(Vector { data: [0.31295744, 0.12929194, 0.9409258], indices: None }) 

## Run

1. Install the qdrant to local

2. Run the qdrant
```bash
docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
``` 

3. Run the client with parameter : the collection name 
```cargo run <collection_name>```

## Result
![Alt text](image-3.png)
![Alt text](image-2.png)